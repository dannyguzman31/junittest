import java.util.Scanner;
import java.util.*;

public class JUnitTest {

    // Factorial formula using
    public static long fact(long n){
         if(n <= 1)
             return 1;
         else
             return n * fact(n-1);
    }

    // check if a word is Palindrome

    public static boolean Palindrome(String pali){

         int i = 0, j = pali.length() -1;

         while ( i < j){
             if ( pali.charAt(i) != pali.charAt(j) )
                 return false;

             i++;
             j--;
         }
        return true;
    }

    public static boolean ArrayEquals(int arr1[], int arr2[]){

        int n = arr1.length;
        int m = arr2.length;

        if(n != m)
            return false;
        Arrays.sort(arr1);
        Arrays.sort(arr2);

        for (int i = 0; i < n; i++){
            if(arr1[i] != arr2[i]){
                return false;
            }
        }
        return true;

    }

    public static int countA(String word){
        int count = 0;
        for(int i = 0; i < word.length(); i++){
            if (word.charAt(i) == 'a' || word.charAt(i) == 'A'){
                count++;
            }
        }
        return count;
    }



    public static void main(String [] args){

        Scanner input = new Scanner(System.in);

        //Test factorial
        System.out.print("Enter number between 1 - 20: ");
        long n = input.nextInt();
        System.out.println("Factorial: " + fact(n));

        // test word count
        System.out.print("Enter Word: ");
        String y = input.next();
        System.out.println("Accurances:" + countA(y));

        // Test Palindrome
        System.out.print("Enter Palindrome: ");
        String z = input.next();
        System.out.println("Palindrome: " + Palindrome(z));

        // Test if arrays are equal
        int arr1[] = {1,2,3,4,5,6,7,8,9};
        int arr2[] = {1,2,3,4,5,6,7,8,9};
        System.out.println("Array Equals: " + ArrayEquals(arr1, arr2));

    }

}
