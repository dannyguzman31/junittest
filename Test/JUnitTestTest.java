import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JUnitTestTest {

            /*
            Assert Methods:
            assertArrayEquals - DONE
            assertEquals - DONE
            assertFalse - DONE
            assertNotNull - DONE
            assertNotSame - DONE
            assertNull - DONE
            assertSame - DONE
            assertThat - not available in Junit 5
            assertTrue - DONE
         */

    @Test
    @DisplayName("Testing Factorial")
    void fact(){
        JUnitTest test1 = new JUnitTest();
        long output = test1.fact(5);
        assertEquals(120, output);
    }

    @Test
    @DisplayName("Testing Palindrome")
    void palindrome(){
        JUnitTest test2 = new JUnitTest();
        boolean output = test2.Palindrome("aba");
        boolean output2 = test2.Palindrome("Java");
        boolean output3 = test2.Palindrome("");
        assertTrue(output);
        assertFalse(output2);
        assertNotNull(output3);

    }

    @Test
    @DisplayName("Testing AssertArrayEquals")
    void ArrayEquals(){
        JUnitTest test3 = new JUnitTest();
        int arr1[] = {1,2,3};
        int arr2[] = {1,2,3};
        int arr3[] = {};
        boolean output = test3.ArrayEquals(arr1,arr2);
        assertArrayEquals(arr1, arr2);
        assertSame(arr1, arr1);
        assertNotSame(arr1, arr3);


    }

    @Test
    @DisplayName("Testing CountA")
    void countA() {
        JUnitTest test3 = new JUnitTest();
        int output = test3.countA("Java");
        assertEquals(2, output);
    }

}